from itertools import groupby
from heapq import *
from bitstring import BitStream, BitArray
#import BitStream, BitArray
#from bitstream import BitStream
#from bitarray import BitArray
import pickle

staticAux = 0

inputFile = open("text.txt", "r")
inputString = inputFile.read()
inputFile.close();

def cmp(a, b):
    return (a > b) - (a < b) 

class Node(object):
	left = None			#0
	right = None		#1
	item = None
	weight = 0

	def __init__(self, i, w):
		self.item = i
		self.weight = w

	def setChildren(self, ln, rn):
		self.left = ln
		self.right = rn

	def __repr__(self):
		return "%s - %s -- %s _ %s" % (self.item, self.weight, self.left, self.right)

	def __cmp__(self, a):
		return cmp(self.weight, a.weight)

	def __lt__(self, a):
		return self.weight < a.weight

itemqueue =  [Node(a,len(list(b))) for a,b in groupby(sorted(inputString))]
heapify(itemqueue)
while len(itemqueue) > 1:					#Make the tree
	l = heappop(itemqueue)
	r = heappop(itemqueue)
	n = Node(None, r.weight+l.weight)		#Make a new node in each iteraction
	n.setChildren(l,r)
	heappush(itemqueue, n)

codes = {}

def codeIt(s, node):						#Make the code based in the tree
	if node.item:
		if not s:
			codes[node.item] = "0"
		else:
			codes[node.item] = s
	else:
		codeIt(s+"0", node.left)
		codeIt(s+"1", node.right)

codeIt("",itemqueue[0])

def huffman(input):
	# above code 

	return codes, "".join([codes[a] for a in input])

code, out = huffman(inputString)

outBinary = BitArray(bin=out)

outfile = open("Converter to Huffmann.bit", "wb")
outBinary.tofile(outfile)
#pickle.dump(int(out, 2), outfile)
outfile.close()

dictionary = open('dictionary.pkl', "wb")
pickle.dump(n, dictionary)
dictionary.close()