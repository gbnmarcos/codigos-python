import pickle
from bitstring import BitStream, BitArray

def cmp(a, b):
    return (a > b) - (a < b) 

class Node(object):
	left = None			#0
	right = None		#1
	item = None
	weight = 0

	def __init__(self, i, w):
		self.item = i
		self.weight = w

	def setChildren(self, ln, rn):
		self.left = ln
		self.right = rn

	def __repr__(self):
		return "%s - %s -- %s _ %s" % (self.item, self.weight, self.left, self.right)

	def __cmp__(self, a):
		return cmp(self.weight, a.weight)

	def __lt__(self, a):
		return self.weight < a.weight

staticAuxIndex = 0
def decodeChar(str, node):
    #returns the char and the length of the binary that represets it
    global staticAuxIndex
    if node.item == None:
        staticAuxIndex += 1
        if str[0] == '0':
            character, staticAuxIndex = decodeChar(str[1:], node.left)
        elif str[0] == '1':
            character, staticAuxIndex = decodeChar(str[1:], node.right)
    else:    
        return(node.item, staticAuxIndex)
    return(character, staticAuxIndex)

def decodeString(str, node):
    global staticAuxIndex
    equivalentString = ''
    while str != "":
        staticAuxIndex = 0
        newChar, toDelete = decodeChar(str, node)
        str = str[toDelete:]
        equivalentString += newChar
    return equivalentString

dictionary = open('dictionary.pkl', 'rb')
n = pickle.load(dictionary)
dictionary.close()


inputBin = BitArray#.BitArray()
#inputIntFile = open('Converter to Huffmann.bit', 'rb')
inputBin = BitArray(filename='Converter to Huffmann.bit')
#inputIntFile.close()
inputString = inputBin.bin


print(decodeString(inputString, n))
