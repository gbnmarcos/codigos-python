import numpy
import math

def compress(uncompressed):
    """Compress a string to a list of output symbols."""
 
    # Build the dictionary.
    dict_size = 256
    dictionary = dict((chr(i), i) for i in range(dict_size))
    # in Python 3: dictionary = {chr(i): i for i in range(dict_size)}
 
    w = ""
    result = []
    for c in uncompressed:
        wc = w + c
        if wc in dictionary:
            w = wc
        else:
            result.append(dictionary[w])
            # Add wc to the dictionary.
            dictionary[wc] = dict_size
            dict_size += 1
            w = c
 
    # Output the code for w.
    if w:
        result.append(dictionary[w])
    #print(dictionary)
    return result, dict_size
 
 
def decompress(compressed):
    """Decompress a list of output ks to a string."""
    from io import StringIO
 
    # Build the dictionary.
    dict_size = 256
    # in Python 2: dictionary = dict((i, chr(i)) for i in range(dict_size))
    dictionary = {i: chr(i) for i in range(dict_size)}
 
    # use StringIO, otherwise this becomes O(N^2)
    # due to string concatenation in a loop
    result = StringIO()
    w = chr(compressed.pop(0))
    result.write(w)
    for k in compressed:
        if k in dictionary:
            entry = dictionary[k]
        elif k == dict_size:
            entry = w + w[0]
        else:
            raise ValueError('Bad compressed k: %s' % k)
        result.write(entry)
 
        # Add w+entry[0] to the dictionary.
        dictionary[dict_size] = w + entry[0]
        dict_size += 1
 
        w = entry
    return result.getvalue()



inputFile = open("texto.txt", "r", encoding="utf8")
inputString = inputFile.read()
inputFile.close();
compressed, size = compress(inputString)

tamanhoOriginalBytes = len(inputString)
tamanhoOriginalBits = tamanhoOriginalBytes*8
print("Tamanho do arquivo original:",tamanhoOriginalBytes ,"Bytes = ",tamanhoOriginalBits , "bits")
numCaracCompressed = len(compressed)
print("Tamanho da informação comprimida: ",numCaracCompressed ,"caracteres")
bitsPerCharCompressed = math.ceil(math.log(size, 2))
print("Número de bits por caractere da informação comprimida: ", bitsPerCharCompressed, "bits")
sizeInBitsCompressed = bitsPerCharCompressed*len(compressed)
print("Tamanho da informação comprimida: ",sizeInBitsCompressed, "bits")
print("Comprimento médio (bits de saída/ caracteres de entrada): ", sizeInBitsCompressed/tamanhoOriginalBytes, "bits por caractere")
print("Taxa de compressão: ", (100*(1-sizeInBitsCompressed/tamanhoOriginalBits)),"%")